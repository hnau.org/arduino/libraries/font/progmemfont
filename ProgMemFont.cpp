#include "ProgMemFont.h"

ProgMemFont::ProgMemFont(
	uint8_t glyphWidth, 
	uint8_t glyphHeight, 
	Glyph firstGlyph, 
	uint8_t glyphsCount, 
	const uint8_t* data
) {
	this->glyphWidth = glyphWidth;
	this->glyphHeight = glyphHeight;
	this->firstGlyph = firstGlyph;
	this->glyphsCount = glyphsCount;
	this->data = data;
	columnBytesCount = (glyphHeight + 7) / 8;
	glyphBytesCount = columnBytesCount * glyphWidth;
}


size_t ProgMemFont::getGlyphWidth() {
	return glyphWidth;
}

size_t ProgMemFont::getGlyphHeight() {
	return glyphHeight;
}
	
bool ProgMemFont::getPixel(Glyph glyph, size_t x, size_t y) {
	if (
		x < 0 || x >= glyphWidth || 
		y < 0 || y >= glyphHeight || 
		!checkGlyphIsAvailable(glyph)
	) {
		return false;
	}
	uint16_t glyphFirstByte = (glyph - firstGlyph) * glyphBytesCount;
	uint16_t offsetBytes = glyphFirstByte + x + y / 8;
	uint16_t offsetBits = y % 8;
	return (pgm_read_byte(data+offsetBytes) >> offsetBits) & 1;
	
}


bool ProgMemFont::checkGlyphIsAvailable(Glyph glyph) {
	return glyph >= firstGlyph && glyph < firstGlyph + glyphsCount;
}
