#ifndef PROGMEMFONT_H
#define PROGMEMFONT_H

#include <Font.h>

class ProgMemFont: public Font {
	
private:

	uint8_t glyphWidth;
	uint8_t glyphHeight;
	Glyph firstGlyph;
	uint8_t glyphsCount;
	const uint8_t* data;
	
	uint8_t columnBytesCount;
	uint8_t glyphBytesCount;
	

public:

	ProgMemFont(
		uint8_t glyphWidth, 
		uint8_t glyphHeight, 
		Glyph firstGlyph, 
		uint8_t glyphsCount, 
		const uint8_t* data
	);
	
	
	virtual size_t getGlyphWidth() override;
	virtual size_t getGlyphHeight() override;
	
	virtual bool getPixel(Glyph glyph, size_t x, size_t y) override;
	virtual bool checkGlyphIsAvailable(Glyph glyph) override;

};


#endif //PROGMEMFONT_H
